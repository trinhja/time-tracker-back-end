package ca.sheridancollege.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQueries({
@NamedQuery(name="Client.getClientList", query="from Client"),
@NamedQuery(name="Client.getClientById", query="from Client where id=:id"),
@NamedQuery(name="Client.getClientByUsername", query="from Client where username=:username"),
@NamedQuery(name="Client.deleteClientById", query="delete from Client where id=:id"),
@NamedQuery(name="Client.byCompanyName", query="from Client where username=:username and companyName like :companyName")
})
public class Client {
	
	@Id
	@GeneratedValue
	private int id;
	private String companyName;
	private String rate;
	private String phone;
	private String address;
	private String website;
	private String username;
	
	public Client(String companyName, String rate, String phone, String address, String website, String username) {
		super();
		this.companyName = companyName;
		this.rate = rate;
		this.phone = phone;
		this.address = address;
		this.website = website;
		this.username = username;
	}
	
}
