package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQueries({
@NamedQuery(name="TimeEntry.getTimeEntryList", query="from TimeEntry"),
@NamedQuery(name="TimeEntry.getTimeEntryById", query="from TimeEntry where id=:id"),
@NamedQuery(name="TimeEntry.getTimeEntryByUsername", query="from TimeEntry where username=:username"),
@NamedQuery(name="TimeEntry.deleteTimeEntryById", query="delete from TimeEntry where id=:id"),
})
public class TimeEntry {
	
	@Id
	@GeneratedValue
	private int id;
	private String startDate;
	private String endDate;
	private String duration;
	private int breakDuration;
	
	@Column(columnDefinition="TEXT")
	private String notes;
	
	private String username;
	
	public TimeEntry(String startDate, String endDate, String duration, int breakDuration, String notes,
			String username) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.duration = duration;
		this.breakDuration = breakDuration;
		this.notes = notes;
		this.username = username;
	}
	
	
	
}
