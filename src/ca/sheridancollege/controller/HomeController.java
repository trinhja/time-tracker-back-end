package ca.sheridancollege.controller;

import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ca.sheridancollege.beans.Client;
import ca.sheridancollege.beans.TimeEntry;
import ca.sheridancollege.dao.DAO;

@RestController
public class HomeController {
	
	private DAO dao = new DAO();
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home(Model model) {
		return "home";
	}
	
	@RequestMapping(value="/clientList", method=RequestMethod.GET)
	public List<Client> getClientList(){
		return dao.getClientList();
	}

	@RequestMapping(value="/clientList/{id}/{companyName}/{rate}/{phone}/{address}/{website}/{username}", method=RequestMethod.POST)
	public void postClientListItem(@PathVariable String companyName, @PathVariable String rate, @PathVariable String phone, @PathVariable String address, @PathVariable String website, @PathVariable String username) {
		dao.postClient(companyName, rate, phone, address, website, username);
	}
	
	@RequestMapping(value="/clientList/body", method=RequestMethod.POST)
	public void postClientJson(@RequestBody Map<String,Object> body) throws Exception {
		String companyName = body.get("companyName").toString();
		String rate = body.get("rate").toString();
		String phone = body.get("phone").toString();
		String address = body.get("address").toString();
		String website = body.get("website").toString();
		String username = body.get("username").toString();
		
		dao.postClient(companyName, rate, phone, address, website, username);
	}
	
//	@RequestMapping(value="/clientList/put", method=RequestMethod.PUT, headers={"Content-type=application/json"})
//	public void putClientList(@RequestBody List<Client> clientList) {
//		dao.putClientList(clientList);
//	}
//	
//	@RequestMapping(value="/clientList/put/{id}/{companyName}/{rate}/{phone}/{address}/{website}/{username}", method=RequestMethod.PUT)
//	public void putClientList(@PathVariable int id, @PathVariable String companyName, @PathVariable String rate, @PathVariable String phone, @PathVariable String address, @PathVariable String website, @PathVariable String username) {
//		dao.putClient(id, companyName, rate, phone, address, website, username);
//	}
	
	@RequestMapping(value="/clientList/delete/{id}", method=RequestMethod.DELETE)
	public void deleteClient(@PathVariable int id) {
		
		dao.deleteClient(id);
	}
	@RequestMapping(value="/clientList/get/{id}", method=RequestMethod.GET)
	public Client getClientList(@PathVariable int id) {
		return dao.getClient(id);
	}
	
	@RequestMapping(value="/clientList/get/username/{username}", method=RequestMethod.GET)
	public List<Client> getClientList(@PathVariable String username) {
		return dao.getClientByUsername(username);
	}
	
	@RequestMapping(value="/clientList/get/search/{username}/{companyName}", method=RequestMethod.GET)
	public List<Client> getClientListByCompanyName(@PathVariable String username, @PathVariable String companyName) {
		return dao.getClientByCompanyName(username, companyName);
	}
//	
//	@RequestMapping(value="/clientList/delete", method=RequestMethod.DELETE)
//	public void deleteClientList() {
//		dao.deleteClientList();
//	}
	
	
	@RequestMapping(value="/timeEntryList", method=RequestMethod.GET)
	public List<TimeEntry> getTimeEntryList(){
		return dao.getTimeEntryList();
	}

	@RequestMapping(value="/timeEntryList/{id}/{startDate}/{endDate}/{duration}/{breakDuration}/{notes}/{username}", method=RequestMethod.POST)
	public void postTimeEntryListItem(@PathVariable String startDate, @PathVariable String endDate, @PathVariable String duration, @PathVariable int breakDuration, @PathVariable String notes, @PathVariable String username) {
		dao.postTimeEntry(startDate, endDate, duration, breakDuration, notes, username);
	}
	
	@RequestMapping(value="/timeEntryList/body", method=RequestMethod.POST)
	public void postTimeEntryJson(@RequestBody Map<String,Object> body) throws Exception {
		String startDate = body.get("startDate").toString();
		String endDate = body.get("endDate").toString();
		String duration = body.get("duration").toString();
		int breakDuration = Integer.parseInt(body.get("breakDuration").toString());
		String notes = body.get("notes").toString();
		String username = body.get("username").toString();
		
		dao.postTimeEntry(startDate, endDate, duration, breakDuration, notes, username);
	}
	
	@RequestMapping(value="/timeEntryList/delete/{id}", method=RequestMethod.DELETE)
	public void deleteTimeEntry(@PathVariable int id) {
		
		dao.deleteTimeEntry(id);
	}
	@RequestMapping(value="/timeEntryList/get/{id}", method=RequestMethod.GET)
	public TimeEntry getTimeEntryList(@PathVariable int id) {
		return dao.getTimeEntry(id);
	}
	
	@RequestMapping(value="/timeEntryList/get/username/{username}", method=RequestMethod.GET)
	public List<TimeEntry> getTimeEntryList(@PathVariable String username) {
		return dao.getTimeEntryByUsername(username);
	}

}
