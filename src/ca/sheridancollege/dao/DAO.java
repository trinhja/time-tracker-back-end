package ca.sheridancollege.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ca.sheridancollege.beans.Client;
import ca.sheridancollege.beans.TimeEntry;

public class DAO {
	
	SessionFactory sessionFactory = new Configuration()
			.configure("ca/sheridancollege/config/hibernate.cfg.xml")
			.buildSessionFactory();
	
	public DAO() {
		super();
	}
	
	//private List<Client> clientList = new CopyOnWriteArrayList<Client>();
	
	public List<Client> getClientList() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Client.getClientList");
		List<Client> clientList = query.getResultList();

		session.getTransaction().commit();
		session.close();
		
		return clientList;
	}
	
	public Client getClient(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Client.getClientById");
		query.setParameter("id", id);
		
		List <Client> clientList = query.getResultList();
		
		Client client = clientList.get(0);
		
		session.getTransaction().commit();
		session.close();
		
		return client;
	}
	
	public List<Client> getClientByUsername(String username) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Client.getClientByUsername");
		query.setParameter("username", username);
		
		List <Client> clientList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return clientList;
	}
	
	
	public List<Client> getClientByCompanyName(String username, String companyName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Client.byCompanyName");
		query.setParameter("username", username);
		query.setParameter("companyName", "%" + companyName + "%");

		List <Client> clientList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return clientList;
	}
	
	public void postClient(String companyName, String rate, String phone, String address, String website, String username) {
		Client client = new Client(companyName, rate, phone, address, website, username);
			
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.saveOrUpdate(client);
		session.getTransaction().commit();
		
		session.close();
	}
	
	public void deleteClient(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Client.deleteClientById");
		query.setParameter("id", id);
		
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
		
	}
	
//	public void putClientList(List<Client> clientList) {
//		this.clientList = new CopyOnWriteArrayList<>(clientList);
//	}
//	
//	public void putClient(int id, String companyName, String rate, String phone, String address, String website, String username) {
//		for (Client c : clientList) {
//			if (c.getId() == id) {
//				c.setCompanyName(companyName);
//				c.setRate(rate);
//				c.setPhone(phone);
//				c.setAddress(address);
//				c.setWebsite(website);
//				c.setUsername(username);
//			}
//		}
//		
//	}
	
//	public void deleteClientList() {		
//	
//		for (int i = 0; i < clientList.size(); i++) {
//			clientList.remove(i);
//			i--;
//			
//		}
//	}
	
	
	public List<TimeEntry> getTimeEntryList() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("TimeEntry.getTimeEntryList");
		List<TimeEntry> timeEntryList = query.getResultList();

		session.getTransaction().commit();
		session.close();
		
		return timeEntryList;
	}

	
	public TimeEntry getTimeEntry(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("TimeEntry.getTimeEntryById");
		query.setParameter("id", id);
		
		List <TimeEntry> timeEntryList = query.getResultList();
		
		TimeEntry timeEntry = timeEntryList.get(0);
		
		session.getTransaction().commit();
		session.close();
		
		return timeEntry;
	}
	
	public List<TimeEntry> getTimeEntryByUsername(String username) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("TimeEntry.getTimeEntryByUsername");
		query.setParameter("username", username);
		
		List <TimeEntry>  timeEntryList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return timeEntryList;
	}
	
	public void postTimeEntry(String startDate, String endDate, String duration, int breakDuration, String notes, String username) {
		TimeEntry timeEntry = new TimeEntry(startDate, endDate, duration, breakDuration, notes, username);
			
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.saveOrUpdate(timeEntry);
		session.getTransaction().commit();
		
		session.close();
	}
	
	public void deleteTimeEntry(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("TimeEntry.deleteTimeEntryById");
		query.setParameter("id", id);
		
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
		
	}
	
}
